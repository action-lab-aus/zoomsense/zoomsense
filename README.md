# ZoomSense

ZoomSense is a cloud-based infrastructure that integrates existing scheduled Zoom meetings. It uses virtual Zoom participants to provide real-time data about attendee participation both inside and outside breakout rooms while recording the video and audio within each breakout room for later analysis. ZoomSense supports the rapid development of external augmentations by using the real-time data captured via the infrastructure.

ZoomSense works by dynamically creating headless Zoom clients on a server who are instructed to join a particular meeting at a specified time. These agents can then be placed into breakout rooms by the host. Once in a breakout room, the client hooks into the `activeSpeaker` events normally used to control who is visible on the client's screen. These are passed in real-time via Firebase Realtime Database to the web-based frontend which visualizes the data for the specific meeting.

# Quick Start Guide

- **Running Your Own ZoomSense Infrastructure**: View the [Quick Start Guide](docs/1_QuickStart_Instructions.md) to get started running your own ZoomSense installation.

- **Develop Augmentations with ZoomSense**: View the [Augmentation Guide](docs/Augmentation.md) to get started with developing augmentations with the help of existing ZoomSense infrastructures.

# Contribution

We welcome new contributors! View our [Contribution Guide](docs/Contribution.md) for details on how to get involved.

# Release Notes

Check out the [Release Notes](docs/ReleaseNotes.md) for latest updates of ZoomSense.

# Current Features and Roadmap

## Current Features

Each ZoomSensor is implemented as a Presentation Foundation application that runs in a Docker container (one per ZoomSensor agent), which will be scheduled by the Docker Manager to actively join meetings and start events monitoring.

Major features of the current system include:

- Enable authentication for creation/monitoring of meetings
- Enable ZoomSensor requests via Zoom and Google calendar integration
- Auto-scheduling for ZoomSensors with Firebase Functions
- Implement Docker Manager to facilitate the creation and deletion of ZoomSensors
- Observe real-time active speaker change events (including silence detection)
- Enable audio and video recordings inside and outside breakout rooms
- Push recordings to Firebase Storage upon leaving the breakout rooms/meetings
- Receive and store chat histories within the Zoom meetings
- Enable `bye bye sensor` feature to kick out sensors within breakout rooms
- Integration with other data sources (e.g. shared Google Documents that students are working on in breakout rooms)

## Roadmap

- Auto-scaling of ZoomSensor agents
- Capture of audio and ML processing to produce live insights
- Inter-participant and participant-agent dialogue to facilitate Zoom meetings
- Structured meetings & automatic summaries for later analysis, presentation, and dissemination
- Support attendee agency & automating breakout room configuration

# Development Team

The current development team has arisen out of the imminent need for this tool for teaching at Monash University, Melbourne. If you want to be involved - get in touch!

- Dr. Tom Bartindale tom.bartindale@monash.edu
- Peter Chen peter.chen@monash.edu
- Dan Richardson dan.richardson@monash.edu
