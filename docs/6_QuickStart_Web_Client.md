[<p align="left">Previous: Configure Firebase CLI</p>](./5_QuickStart_Firebase_CLI.md)

# 5. Deploy the ZoomSense Web Client

This section provides instructions on how to deploy the ZoomSense Web Client. ZoomSense Web Client is a static OPA to display data collected by ZoomSense agents operating inside specified Zoom meetings, allowing for greater insight into what's happening in your Zoom meetings and their break-out rooms (powered by Vue.js and Firebase).

## 5.1 Clone the Source Code

The source code of ZoomSense Web Client can be found under the [ZoomSense Web Client Repository](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-web-client)

Clone the repository:

```
git clone https://gitlab.com/action-lab-aus/zoomsense/zoomsense-web-client
```

Install dependencies:

```
npm install
```

Here is an overview of the repository structure:

```
.
├── LICENSE
├── README.md
├── babel.config.js
├── firebase.json
├── package-lock.json
├── package.json
├── public
│   ├── favicon.png
│   └── index.html
├── src
│   ├── App.vue
│   ├── assets
│   │   ├── background.png
│   │   ├── google_hover.png
│   │   ├── google_normal.png
│   │   └── google_pressed.png
│   ├── components
│   │   ├── DocumentProgress.vue
│   │   ├── GDocTile.vue
│   │   ├── GoogleDocSection.vue
│   │   ├── LineGraph.vue
│   │   ├── Loader.vue
│   │   ├── Meetings.vue
│   │   ├── Network.vue
│   │   ├── RoomSpeakers.vue
│   │   ├── ScheduledCall.vue
│   │   ├── SpeakerPie.vue
│   │   ├── Timeline.vue
│   │   └── event-bus.js
│   ├── db.js
│   ├── main.js
│   ├── quasar.js
│   ├── router
│   │   └── index.js
│   ├── scripts
│   │   └── gapi.js
│   ├── shared.js
│   ├── styles
│   │   ├── quasar.sass
│   │   └── quasar.variables.sass
│   └── views
│       ├── About.vue
│       ├── Admin.vue
│       ├── Anonymous.vue
│       ├── Home.vue
│       ├── Login.vue
│       ├── MeetingDetails.vue
│       └── docs
│           ├── AddDoc.vue
│           └── ManageDocs.vue
└── vue.config.js
```

You can deploy the ZoomSense Web Client in any hosting environment that you are familiar with. Here, we use Firebase Hosting to deploy the application.

## 5.2 Set up Firebase Function Configs for Zoom OAuth Credentials

Run `firebase use --add` to define the project alias to be used for the Web Client (set the alias to be **default**). Find and choose the Firebase Project created in [Step 1.1](./2_QuickStart_Firebase.md).

Execute the following command to set up Firebase function configs with **Zoom OAuth Credentials** you obtained via [Step 2.2](./3_QuickStart_Zoom_OAuth.md).

```
firebase functions:config:set FIREBASE_PROJECT_ID.zoom.client_id="Zoom OAuth ID" FIREBASE_PROJECT_ID.zoom.client_secret="Zoom OAuth Secret"
```

## 5.3 Create a Firebase Web App

Before deploying the ZoomSense Web Client to Firebase, we need to create a Firebase Web App to get started.

<img src="../img/quickstart/5_deploy_web_client/5.3.1_create_web_app.png" alt="Create Firebase Web App" width="800"/>

Register the app by providing the App nickname. Choose **Also set up Firebase Hosting for this app** to get Firebase Hosting setup for you.

<img src="../img/quickstart/5_deploy_web_client/5.3.2_register_app.png" alt="Register Web App" width="800"/>

Choose **Continue** for all the remaining sections, and you will be directed back to the **Project settings** tab. Under **Your app**, you will see a Web App created successfully. Choose **Config** under the **SDK setup and configuration** section to get the Firebase configuration object containing keys and identifiers for your app.

<img src="../img/quickstart/5_deploy_web_client/5.3.3_web_app.png" alt="Web App Config" width="800"/>

## 5.4 Set up the Firebase Configurations

Update the `.env.production` & `.env.development` files with the configuration details received in the previous step as:

```
VUE_APP_REDIRECT=https://FIREBASE_PROJECT_ID.web.app
VUE_APP_FB_KEY=firebaseConfig.apiKey
VUE_APP_FB_AUTH=firebaseConfig.authDomain
VUE_APP_RTDB=firebaseConfig.databaseURL
VUE_APP_FB_ID=firebaseConfig.projectId
VUE_APP_FB_BUCKET=firebaseConfig.storageBucket
VUE_APP_FB_FUNCTIONS=https://us-central1-FIREBASE_PROJECT_ID.cloudfunctions.net
VUE_APP_MESSAGING_SENDER_ID=firebaseConfig.messagingSenderId
VUE_APP_APP_ID=firebaseConfig.appId
```

These environment variables will then be loaded into `src/db.js` for initializing the Firebase App for the Web Client.

## 5.5 Build and Deploy the Web Client

Make sure under the `.firebaserc` file, the default project has been set to the Firebase Project you have created as:

```
{
  "projects": {
    "default": "zoomsense-acmmm"
  }
}
```

Build the Web Client and deploy it to Firebase Hosting:

```
npm run build

firebase deploy
```

[<p align="right">Next: Configure GCP IAM Permissions</p>](./7_QuickStart_IAM.md)
