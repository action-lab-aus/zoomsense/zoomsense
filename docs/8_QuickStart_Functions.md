[<p align="left">Next: Configure GCP IAM Permissions</p>](./7_QuickStart_IAM.md)

# 7. Deploy Firebase Functions and Security Rules

This section provides instructions on how to deploy the ZoomSense Firebase Functions with Security Rules. Firebase Functions act as the serverless backend for the ZoomSense infrastructure. These functions utilise external APIs, storing information in the project's Firebase Realtime Database (RTDB), which is encrypted at rest.

## 7.1 Clone the Source Code

The source code of Firebase functions and rules can be found under the [**ZoomSense Function Repository**](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-functions).

Clone the repository:

```
git clone https://gitlab.com/action-lab-aus/zoomsense/zoomsense-functions.git
```

Install dependencies:

```
cd functions

npm install
```

Here is an overview of the repository structure:

```
.
├── LICENSE
├── README.md
├── database.json
├── firebase.json
├── functions
│   ├── functions
│   │   ├── anonymous.js
│   │   ├── auth.js
│   │   ├── chat.js
│   │   ├── gdocs.js
│   │   ├── prompt.js
│   │   ├── scheduler.js
│   │   └── transcoder.js
│   ├── index.js
│   ├── package-lock.json
│   ├── package.json
│   └── utils
│       └── timeformatter.js
└── storage.rules
```

## 7.2 Configure Environment Variables for Firebase Functions

Run `firebase use --add` to define the project alias to be used for the Firebase Function (set the alias to be **default**). Find and choose the Firebase Project created in [Step 1.1](./2_QuickStart_Firebase.md).

Create a `.runtimeconfig.json` file under the root directory of the function repository. Copy the following JSON object into the file and update it with the credentials you received from the previous steps.

- `zoom.client_id` & `zoom.client_secret`: [Step 2.1](./3_QuickStart_Zoom_OAuth.md)
- `firebase_credentials`: [Step 1.2](./2_QuickStart_Firebase.md)
- `gapi`: Credentials can be found under the Credentials sections in [GCP APIs and Services](https://console.developers.google.com)

```JSON
{
    "firebase_rtdb": "https://FIREBASE_DATABASE_URL.firebaseio.com/",
    "site_url": "https://FIREBASE_PROJECT_ID.web.app",
    "functions_url": "https://us-central1-FIREBASE_PROJECT_ID.cloudfunctions.net/",
    "localhost_functions_url": "http://localhost:5000/FIREBASE_PROJECT_ID/us-central1/",
    "zoom": {
        "client_id": "Zoom OAuth App Client ID",
        "client_secret": "Zoom OAuth App Client Secret"
    },
    "firebase_credentials": {
        "type": "service_account",
        "project_id": "*******",
        "private_key_id": "*******",
        "private_key": "-----BEGIN PRIVATE KEY-----********\n-----END PRIVATE KEY-----\n",
        "client_email": "*******@appspot.gserviceaccount.com",
        "client_id": "*******",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/*******%40appspot.gserviceaccount.com"
    },
    "gapi": {
        "client_id": "Google OAuth 2.0 Client ID",
        "client_secret": "Google OAuth 2.0 Client Secret",
        "api_key": "Browser Key"
    },
    "anonymous": {
        "encryption_key": "Encryption Key for Anonymous Sign-in (Any String)",
        "encryption_secret": "Encryption Secret for Anonymous Sign-in (Any String)"
    }
}
```

Set environment configuration for your project by using the `firebase functions:config:set` command in the Firebase CLI. More details can be found on [Environment Configuration](https://firebase.google.com/docs/functions/config-env).

For example, you can run the following command on Mac/Linux to set environment configuration from the `.runtimeconfig.json` file (please replace the `FIREBASE_PROJECT_ID` with your Firebase Project ID below):

```
firebase functions:config:set FIREBASE_PROJECT_ID="$(cat .runtimeconfig.json)"
```

## 7.3 Deploy Firebase Functions & Rules

The Firebase Function repository also contains Firebase Realtime Database rules and Storage rules for secured data access:

- **Realtime Database Rules**: `database.json`
- **Storage Rules**: `storage.rules`

To deploy the Firebase Function:

```
firebase deploy
```

[<p align="right">Next: Configure Docker Manager & Windows Sensors</p>](./9_QuickStart_Docker.md)
