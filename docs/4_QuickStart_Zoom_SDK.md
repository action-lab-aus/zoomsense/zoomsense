[<p align="left">Previous: Zoom OAuth App</p>](./3_QuickStart_Zoom_OAuth.md)

# 3. Generate **Zoom SDK** Key and Secret

This section provides detailed instructions on how to Create a Zoom SDK app on Zoom Marketplace. The SDK App will be used by the Windows ZoomSensors to integrate a rich subset of features that are available in the Zoom Client application.

Under the **Develop** tab, select **Build App**. Choose SDK to start creating an SDK App on Zoom. Provide basic information and developer details under the **Information** section including:

- Company Name
- Developer Contact Name
- Developer Contact Email Address

<img src="../img/quickstart/3_zoom_sdk/3.1_sdk_info.png" alt="Zoom SDK App Basic Information" width="800"/>

Under the **App Credentials** section, you will be able to find the SDK Key & Secret for developing the ZoomSensor application.

<img src="../img/quickstart/3_zoom_sdk/3.2_sdk_credentials.png" alt="Zoom SDK App Credentials" width="800"/>

[<p align="right">Next: Configure Firebase CLI</p>](./5_QuickStart_Firebase_CLI.md)
