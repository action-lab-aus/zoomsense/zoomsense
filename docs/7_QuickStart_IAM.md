[<p align="left">Previous: Deploy ZoomSense Web Client</p>](./6_QuickStart_Web_Client.md)

# 6. Configure GCP IAM Permissions

This section provides instructions for configuring GCP IAM Permissions to allow the Firebase Admin SDK to create OAuth2 access tokens for user authentication purposes.

## 6.1 Enable IAM API for the Project

Find the Project ID under **Project setting**, and go to `https://console.developers.google.com/apis/api/iam.googleapis.com?project=zoomsense-acmmm` to enable the IAM API for your project (replace `zoomsense-acmmm` with your project's ID).

<img src="../img/quickstart/6_iam/6.1_iam_api.png" alt="Enable IAM API" width="800"/>

## 6.2 Add Roles for the App Engine Default Service Account

Search for the IAM & Admin page on the Google Cloud Console:

<img src="../img/quickstart/6_iam/6.2.1_iam_admin.png" alt="IAM & Admin" width="800"/>

Fine the App Engine Default Service Account as `FIREBASE_PROJECT_ID@appspot.gserviceaccount.com`, and click the edit button to edit permissions. Assign the **_Service Account Token Creator_** and **_Editor_** role to your App Engine Default Service Account:

<img src="../img/quickstart/6_iam/6.2.2_edit_permissions.png" alt="Edit Permissions" width="800"/>

[<p align="right">Next: Deploy Firebase Functions</p>](./8_QuickStart_Functions.md)
