[<p align="left">Previous: Firebase Project</p>](./2_QuickStart_Firebase.md)

# 2. Generate **Zoom OAuth** key and secret

This section provides detailed instructions on how to Create a Zoom OAuth app on Zoom Marketplace. The OAuth App will be used by the ZoomSense Web Client to view and manage user's meetings and basic profiles.

#### YOU WILL USE THIS ACCOUNT DURING A LATER STEP.

## 2.1 Create a Zoom Account

Go to [Zoom MarketPlace](https://marketplace.zoom.us/) and create a Zoom account.

## 2.2 Create a **Zoom OAuth** App

Under the **Develop** tab, select **Build App**.

<img src="../img/quickstart/2_zoom_oauth/2.2.1_zoom_build_app.png" alt="Zoom App Marketplace - Build App" width="800"/>

Choose Oauth to start creating an OAuth App on Zoom:

- Provide the OAuth App Name
- Choose the app type to be **User-managed app**
- Deselect publishing to Zoom Marketplace
- Click **Create** to create the OAuth App

<img src="../img/quickstart/2_zoom_oauth/2.2.2_zoom_oauth.png" alt="Zoom OAuth App" width="800"/>

You will be provided with Zoom OAuth Credentials for accessing Zoom APIs. Under the **App Credentials** section, fill in the **Redirect URL for OAuth** & **Add Whitelist URLs** with:

```
https://us-central1-FIREBASE_PROJECT_ID.cloudfunctions.net/authtokenredirect
```

The FIREBASE_PROJECT_ID can be found in the Firebase console, under **Project settings**'s **General** section.

<img src="../img/quickstart/2_zoom_oauth/2.2.3_oauth_credentials.png" alt="Zoom OAuth App Credentials" width="800"/>

Leave the default details under the **Information** and **Feature** section, and go to the **Scopes** section. Add the following scopes via the console so that we will be able to retrieve the user and meeting details on behalf of the user:

- `meeting:read`
- `meeting:write`
- `user:read`
- `user_profile`

<img src="../img/quickstart/2_zoom_oauth/2.2.4_oauth_scopes.png" alt="Zoom OAuth Scopes" width="800"/>

[<p align="right">Next: Zoom SDK App</p>](./4_QuickStart_Zoom_SDK.md)
