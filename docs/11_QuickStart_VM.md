[<p align="left">Next: Schedule a ZoomSense Meeting</p>](./10_QuickStart_Schedule.md)

# 10. Configure the Live Environment (Optional)

This section provides instructions for migrating the ZoomSense infrastructure to a live environment via Windows Google Cloud VM. However, any Windows instances with Docker installed can be used to host the Docker Manager and Windows Sensor logic.

## 10.1 Create a Windows Instance

To run the ZoomSense infrastructure in the live environment, you can use Windows Cloud VMs to achieve that.

For this QuickStart Guide, we use **Windows Server 2019 Datacenter Core for Containers** (with Docker pre-installed) on Google Cloud. However, you can use any other Windows instance provider to achieve the same outcome.

Under the Google Cloud Console, go to the **Compute Engine** tab and click **Create Instance**:

<img src="../img/quickstart/10_live/10.1.1_compute_engine.png" alt="Create Instance" width="800"/>

Fill in the details of the VM instance including Name, Region, Machine Type (**the minimum VM spec to support ZoomSense Infrastructure will be 2 vCPU, 7.5 GB Memory**).

<img src="../img/quickstart/10_live/10.1.2_instance_details.png" alt="Configure Instance Details" width="800"/>

Under the **Boot disk** section, change the Operating System to **Windows Server** and use the **Windows Server 2019 Datacenter Core for Containers** version for the instance.

<img src="../img/quickstart/10_live/10.1.3_image.png" alt="Boot Disk Image" width="800"/>

## 10.2 Connect to the VM

Once the VM has been created, under **Connect**, download the RDP file and set the Windows password for the access.

<img src="../img/quickstart/10_live/10.2_connect.png" alt="Connect to the Instance" width="800"/>

## 10.3. Enable Named Pipe with Docker Daemon

In (each of) our Windows environment(s), modify the Docker daemon.json file to enable named pipe. To configure the Docker daemon using a JSON file, create a file at `C:\ProgramData\docker\config\daemon.json` on Windows. The configuration file looks like this:

```JSON
{
  "registry-mirrors": [],
  "insecure-registries": [],
  "experimental": false,
  "hosts": [
    "npipe://",
    "tcp://0.0.0.0:2375"
  ]
}
```

<img src="../img/quickstart/10_live/10.3_named_pipe.png" alt="Enable Named Pipe" width="800"/>

Changes to this `daemon.json` file will not come into effect until the machine restarts.  You can put off restarting 
for now and do the next steps first however, especially if you're planning on adding Windows Explorer and the Control 
Panel in step 10.8 (which also requires a restart).

```shutdown /r```

## 10.4 Add Custom Metadata

To start the docker service and Docker Manager when the Windows instance restarts, you need to add a [Custom Metadata](https://cloud.google.com/compute/docs/storing-retrieving-metadata#custom).

Edit your VM instance and under the **Custom metadata** section, and a new item with:

- Key: `windows-startup-script-ps1`
- Value:

```
restart-service *docker*
cd C:\Users\YOUR_VM_USERNAME\Documents
docker container rm documents_manager_1
docker-compose up -d
```

<img src="../img/quickstart/10_live/10.4_custom_metadata.png" alt="Add Custom Metadata" width="800"/>

## 10.5 Configure Environment Variables

Under `C:\Users\YOUR_VM_USERNAME\Documents`, create an `env.env` file to store the Environment Variable for ZoomSense (same as [Step 8.3](./9_QuickStart_Docker.md)):

```
FirebaseUrl=https://FIREBASE_DATABASE_URL.firebaseio.com
ZoomsenseUrl=https://FIREBASE_PROJECT_ID.web.app/#
ZoomAppKey=Zoom SDK App Key
ZoomAppSecret=Zoom SDK App Secret
FirebaseKey=Firebase App Key
FirebaseAuthEmail=Firebase Admin Account Auth Email (Step 8.2)
FirebaseAuthPwd=Firebase Admin Account Auth Password (Step 8.2)
FirebaseBucketUrl=FIREBASE_PROJECT_ID.appspot.com
DockerHost=Public IPv4 Address
DockerPort=2375
DockerImage=zoomsense/windowssensor:latest
ProcessRecordings=false
FirebaseAdmin=Firebase Admin Service Account Credentials
```

## 10.6 Install Docker Compose

Install Docker compose if it is not installed on your VM instance. If you are using Windows Servers, you can install Docker Compose via the following PowerShell command:

```
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

Invoke-WebRequest "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-Windows-x86_64.exe" -UseBasicParsing -OutFile $Env:ProgramFiles\Docker\docker-compose.exe
```

## 10.7 Configure Docker Compose

Copy the `docker-compose.yaml` file from [ZoomSense Docker](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-docker).

Pull the Windows Sensor Docker image with the following command to use the pre-built Docker image:

```
docker pull zoomsense/windowssensor:latest
```

## 10.8 [Optional] Add Windows Explorer and Control Panel

The image used above is very minimal.  In order to add Explorer and a Control Panel to make changes to the machine 
that aren't easy in the cmd shell or PowerShell, run the following command in PowerShell:

```
Add-WindowsCapability -Online -Name ServerCore.AppCompatibility~~~~0.0.1.0
```

A restart is required after this command completes.

```
shutdown /r
```

## 10.9 [Optional] Start the Docker Manager

This step requires that your VM has been restarted since you edited the `daemon.json` file, and if you've done step 
10.4 then the docker manager will start automatically whenever the VM starts up.  However, if you ever want to start 
the Docker Manager in your Windows VM manually, run:

```
docker-compose up -d
```

Please note the first time the Docker Manager runs, it will take a bit of time to download the docker image of the 
`zoomsense/zoomsense-manager` container.