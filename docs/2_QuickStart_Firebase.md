[<p align="left">Previous: Instructions</p>](./1_QuickStart_Instructions.md)

# 1. Create a Firebase Project

This section provides an overview of how to create a Firebase Project for ZoomSense with a Realtime Database, Storage, Authentication. Instructions for creating Firebase Admin SDK credentials and upgrading the Firebase Billing Plan will also be covered in this section. The Firebase Project will be used as the serverless backend for handling data storage, real-time event triggers, hosting, and authentication.

#### YOU WILL USE THIS ACCOUNT DURING A LATER STEP.

## 1.1 Add a new Firebase project

Create a new Firebase Project in the [online console](https://console.firebase.google.com/u/0/).

<img src="../img/quickstart/1_firebase/1.1_add_project.png" alt="Add a new Firebase Project" width="500"/>

## 1.2 Create Firebase Admin SDK credentials

Under the Firebase console, go to `Settings > Service Accounts`. Under the **Firebase Admin SDK** section, click `Generate new private key` to create a Firebase service account that can be used to authenticate Firebase features programmatically.

<img src="../img/quickstart/1_firebase/1.2_admin_sdk.png" alt="Admin SDK" width="800"/>

## 1.3 Enable Firebase Realtime Database

Under the **Build** section, choose **Realtime Database** and click `Create Database` to create the default Realtime Database. We have tested that creating the default Realtime Database in `us-central1` is definitely working. However, setting the default RTDB in other locations might require extra configurations during the function deployment.

<img src="../img/quickstart/1_firebase/1.3.1_rtdb.png" alt="Realtime Database" width="800"/>

Choose `Start in Test Mode` to enable a quick setup for the database.

<img src="../img/quickstart/1_firebase/1.3.2_rtdb_security.png" alt="Realtime Database Security Rules" width="800"/>

## 1.4 Enable Firebase Storage

Under the **Build** section, choose **Storage** and click `Get started` to create the default Firebase Storage.

<img src="../img/quickstart/1_firebase/1.4.1_storage.png" alt="Firebase Storage" width="800"/>

Use the default security rules and set the Cloud Storage location based on your needs.

<img src="../img/quickstart/1_firebase/1.4.2_storage_security.png" alt="Firebase Storage Security Rules" width="800"/>

## 1.5 Enable Firebase Authentication

Under the **Build** section, choose **Authentication** and click `Get started` to enable Firebase Authentication.

<img src="../img/quickstart/1_firebase/1.5.1_auth.png" alt="Firebase Authentication" width="800"/>

Under the **Sign-in Method** tab, enable the following sign-in providers:

- Email/Passoword
- Google
- Anonymous

<img src="../img/quickstart/1_firebase/1.5.2_auth_signin.png" alt="Firebase Authentication Sign-in Method" width="800"/>

### Create at least 1 local account with a custom claim of `admin:true` [**Custom Claims**](https://firebase.google.com/docs/auth/admin/custom-claims)

## 1.6 Upgrade the Firebase Project's Billing Plan

In order to use the Firebase Functions, you will need to upgrade your billing plan for the Firebase Project to be the **Blaze Plan (Pay as you go)**.

At the bottom of the left panel on the Firebase console, click **Upgrade** to upgrade your Firebase billing plans.

<img src="../img/quickstart/1_firebase/1.6_billing.png" alt="Firebase Billing Plan" width="800"/>

[<p align="right">Next: Zoom OAuth App</p>](./3_QuickStart_Zoom_OAuth.md)
