# Quick Start Guide

ZoomSense is a distributed service architecture spanning muliple providers:

### Google Firebase

To deploy the full ZoomSense infrastructure on your own, you need to have a [Firebase Project](https://firebase.google.com/) with **Blaze Plan (Pay as you go)** to use the Firebase services including:

- Firebase Realtime Database
- Firebase Functions
- Firebase Storage
- Firebase Authentication
- Firebase Hosting

### Zoom

A proprietary software - Zoom, needs to be integrated to allow meeting scheduling, data collection and analysis. You need to have a Zoom account to create both a **Zoom OAuth Application** & a **Zoom SDK Application** (C#) via the [Zoom Marketplace](https://marketplace.zoom.us/) to enable the services. Both the OAuth and SDK applications are free to use with Zoom's Basic Plan with certain [limitations](https://zoom.us/pricing).

### Windows Docker

Since ZoomSense uses headless Windows Zoom clients for joining and monitoring Zoom meetings, at least one Windows VM or local Windows machine running Docker is also needed.

We have provided a step-by-step guide to help get everything running:

[<p align="right">Next: Setting up a Firebase Project</p>](./2_QuickStart_Firebase.md)

## 📗📗📗 For ACM Multimedia Open Source Competition 📗📗📗

We understand that installing the entire infrastructure from scratch can take some time. If you find any difficulties with running the ZoomSense infrastructure, please feel free to contact peter.chen@monash.edu or tom.bartindale@monash.edu for help.

Alternatively, we have also set up a testing infrastructure for ACMMM's review process. If you would like to explore the example augmentation scenarios on top of the ZoomSense infrastructure, please contact us so we can share the test account with you.

## System Architecture Overview

ZoomSense infrastructure has four major components:

- **ZoomSense Windows Client**: https://gitlab.com/action-lab-aus/zoomsense/zoomsense-windows (`docker pull zoomsense/windowssensor:latest` to use the built Docker image)

- **ZoomSense Docker Scheduler**: https://gitlab.com/action-lab-aus/zoomsense/zoomsense-docker (`docker pull zoomsense/zoomsense-manager:latest` to use the built Docker image)

- **ZoomSense Google Cloud Functions**: https://gitlab.com/action-lab-aus/zoomsense/zoomsense-functions

- **ZoomSense Web Client**: https://gitlab.com/action-lab-aus/zoomsense/zoomsense-web-client

More details of the System Architecture can be found with the following diagram:

![ZoomSense System Architecture](../img/ZoomSense.png)

[<p align="right">Next: Setting up a Firebase Project</p>](./2_QuickStart_Firebase.md)
