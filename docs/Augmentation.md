# Augmentation

ZoomSense supports "Configuration Driven" design through Firebase RTDB to allow easy augmentation of existing ZoomSense deployments in three ways.

The Augmentation Guide provides a showcase of the diverse contexts in which ZoomSense can be used to develop prototypes with a selection of examples from our research collaborators. An overview of the Firebase RTDB data structure is also introduced to provide developers with a better understanding of how to join the rapid and flexible development of ZoomSense augmentations.

Since Firebase provides cross-platforms SDKs to help build applications on Web, Android, iOS, C++, and Unity, any software that can connect to Firebase will be able to develop augmentations on top of existing ZoomSense infrastructures.

## 1. System Wide Augmentation

System Wide Augmentation allows people to gain Developer Access to the Firebase project of an existing ZoomSense infrastructure to implement new functionalities.

### 1.1 Multi-Modal Analytics

Research implementation using Firebase Functions and VueJS that creates structured Google Docs and visualises progress available as part of the manager web client:

- [ZoomSense Web Client](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-web-client)

### 1.2 Supporting Attendee Agency & Automating Room Configuration

Reference implementation using Zoom Android SDK as an alternative ZoomSensor to manipulate BO rooms via Firebase:

- [ZoomSense Breakout Room Facilitator](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-bo-facilitator)

## 2. Host Meeting Augmentation

Since each meeting host has full access to the Firebase RTDB node using their own credentials, developers can augment an individual host's meeting using just their own access credentials.

### 2.1 Inter-Participant or Participant-Agent Dialogue

Student project implementation using only VueJS of a Bot that facilitates ice-breaker activities in BO rooms:

- [Zoomsense Icebreaker](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-icebreaker)

### 2.2 Structured Meetings & Automatic Summaries

Research implementation using Firebase Functions where an external meeting agenda produces meta-data that drives automatic video summaries of meetings:

- [Structured Meetings](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-functions/-/tree/feature/agenda)

## 3. Data Consumption Augmentation

With the anonymous authentication feature in ZoomSense, developers can use the anonymous access token to allow any third-party applications to have read-only access directly to the RTDB to obtain data for a specific meeting.

### 3.1 Learning Activity Analytics

Research implementation using Firebase Functions and VueJS visualising live and historical activity data specifically designed to support tutors managing online classes:

- [ZoomSense Web Client (Learning Analytics)](https://gitlab.com/Stalkcomrade/zoomsense-web-client)

### 3.2 Live On-Screen Title Integration

Production implementation using only VueJS, where chat messages can be displayed onto the host’s Zoom video as an overlay in real-time:

- [Open Titler GitHub Repo](https://github.com/tombartindale/opentitler)
- [Open Titler App](https://opentitler.web.app/#/)

### 3.3 Example Application for Data Consumption Augmentation

Example Vue.js application implementing a Data Consumption Augmentation (live read-only integration) with a ZoomSense deployment using just an anonymous access share token.

- [Vue.js ZoomSense Example](https://gitlab.com/action-lab-aus/zoomsense/vue-zoomsense-example)
- [`vue-zoomsense` NPM Library](https://gitlab.com/action-lab-aus/zoomsense/vue-zoomsense)

# Data Structure

The following section provides an overview of the ZoomSense Firebase RTDB data structure. Please check the `sample.json` file under the repository to get a better understanding of each object node.

- Coordinated Universal Time (UTC) is used for all the date-time elements
- `ZoomSensor_1` works as the manager bot (e.g. send web client URLs and record the main session's active speaker data)

```
- data
  - activeSpeakers
    - meetingId_timestamp
      - sensorId # e.g. ZoomSensor_1, ZoomSensor_2
        - current # current meeting session (join or leave breakout rooms will change the meeting session)
        - history # history meeting session
          - sessionEndTime # history node is differentiated by the end time of each meeting session
            - activeHistory # actual active speaker history data
              - activeHistoryId
                - timestamp # timestamp of the active speaker event
                - zoomidList # users for the active speaker event, can be null or a list of zoom IDs
            - isInBO # whether the current session is in the breakout room
            - hasRecording # whether the session has been recorded
            - isRecording # whether the session is being recorded now
            - userList # user list for the current session
              - userId
                - userRole # Attendee, Host, or Co-host
                - userName # display name of the user in Zoom

  - chats
    - meetingId_timestamp
      - sensorId
        - chatId
          - isInBO # whether the chat message is received within the breakout room
          - msg # message content
          - msgReceiver # message receiver's zoomid
          - msgReceiverName # message receiver's display name
          - msgSender
          - msgSenderName
          - msgType # type of the prompt message (silenceNoDoc, silenceWithDoc, personNotTalkingActivePrompt, personNotTalkingDirectPrompt, personTalkingTooMuch)
          - timestamp
        - leave
          - leave # commands to be executed for sensor to leave the breakout room
        - message
          - receiver # receiver of the message, 0 indicates sending to all participants
          - msg # content of the message
        - isInBO # bo flag
    - prompts
      - message: type # list of prompt messages with type

  - gdocs
    - meetingId_timestamp
      - sensorId
        - driveFolderId # The ID of the Google Drive folder for this room's documents
        - documentId # The ID of the parent Google Doc
          - driveCopyId # The ID of this room's copy of the parent Google Doc
          - revisions
            - revisionId # Given by Google Drive
              - modifiedAt
              - modifiedBy # Bug in Drive API means that this is currently the original document creator
                - displayName
                - emailAddress
              - responses
                - sectionIndex # Responses for that section as it existed at time of the update

  - logs
    - entryExit
      - meetingId_timestamp
        - sensorId
          - entryExitId
            - uid # Zoom uid (cannot be used to uniquely identify users since when user enter/leave breakout rooms, the uid will be re-assigned)
            - userName # display name of the user
            - userRole # user role (however, in breakout rooms, host/co-hosts will all be identified as attendees)
            - status # leave/enter
            - isInBO
            - timestamp

- meetings
  - uid # Firebase authentication uid to ensure each user can only access their own meeting data
    - meetingId_timestamp
      - actualEndTime # actual end time of the meeting
      - actualStartTime # actual start time of the meeting
      - endTime # meeting end time retrieved from Zoom
      - noOfBreakoutRooms # number of breakout rooms requested
      - scheduled # whether the meeting has been scheduled
      - startTime # meeting start time retrieved from Zoom
      - topic # topic of the meeting (e.g. FIT5125 S1 2020 Week 11 Workshop)
      - version # version for the Windows Sensor
      - deleted # indicates the time when the underlying zoom meeting was deleted
      - vmAllocation
        - sensorId # record the vm allocated for the sensor
      - sensorId # meeting session reference for all data sources
        - sessionEndTime
          - isInBO
      - gdocs
        - driveFolderId # ID of the Google Drive folder for this meeting
        - documents
          - documentId
            - title
            - distributed # boolean - has the document been shared to the breakout rooms?
            - googleAccount # ID of the Google Account that created the document
            - sections
              - sectionIndex
                - data
                  - title # section title
                  - content # section's written information/instruction
                  - response # boolean - if a box for a written response should be provided

- scheduling
  - ip # ipv4 address of the windows cloud VM or local desktop
    - capacity # maximum number of sensors run in parallel with recording
    - meetingId_timestamp-sensorId
      - lastSeen # last active time of zoom sensors to check whether they are still working
      - scheduled # whether this sensor has been scheduled
      - startTime # scheduled start time of the sensor
      - uid # uid of the meeting owner

- anonymous
  - tokens # tokens generated for anonymous login
    - hostId
      - meetingId_timestamp
        - token # once the token is deleted, the anonymous access will be denied
  - users
    - uid # anonymous user id
      - hosts # received anonymous access from which host
        - hostId
      - meetings # received anonymous access from which meeting
        - meetingId_timestamp

- processing
  - recordings # processing queue for transcoding the recordings
    - vm # vm allocated for the transcoding
      - queueId
        - fileBucket # bucket for the raw recording files
        - filePath # path of the raw recording files
        - created

- whitelist # a list of email addresses whitelisted for using ZoomSense

- gdocsTokens
  - uid # ZoomSense Account ID
    - uid # Google Account ID -- can link multiple per ZoomSense Account
        - email
        - name
        - picture # URL to image
        - token # OAuth token for Google APIs--only given once! If lost, user needs to delete ZoomSense from Google Account and re-link.

- zoomtokens # authentication tokens for using ZoomSense
  - uid # ZoomSense account ID
    - access
    - access_expires
    - auth
    - refresh
```

## Recordings

When enabled, recordings are uploaded to a Firebase bucket under `<bucket>/<userid>/<meetingid_timestamp>/<zoomsensorname>-<sessionEndTime>/*.meetingrec`.

## Push Back Inferred ZoomSense Data

Developer accounts with write access to Firebase can push inferred data back to help contribute to ZoomSense development. More information about how to contribute with ZoomSense can be found under the [Contribution Guide](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/Contribution.md).

# Data Access Level for Different Types of Augmentation

## 1. System Wide Augmentation

For System Wide Augmentation, developers will have full READ and WRITE access to the Firebase RTDB data.

## 2. Host Meeting Augmentation

With Host Meeting Augmentation, each meeting host has full READ and WRITE access to the Firebase RTDB node using their own credentials. Firebase Rules will check whether the `meetingId_timestamp` exists under the user's `meetings` node to allow or deny access for data under the `data` node.

## 3. Data Consumption Augmentation

Data Consumption Augmentation is supported by the anonymous authentication feature in ZoomSense. Developers can use the anonymous access token to have READ-ONLY access directly to the RTDB to obtain data for a specific meeting. READ access for meeting details under `meetings/uid/meetingId_timestamp` will also be granted.
