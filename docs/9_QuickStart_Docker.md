[<p align="left">Next: Deploy Firebase Functions</p>](./8_QuickStart_Functions.md)

# 8. Configure Docker Manager & Windows Sensors (Local Environment)

This section provides instructions on how to configure the Docker Manager and Windows Sensors on a local Windows Environment. Docker Manager is a node script powered by Firebase Realtime Database and Docker API which performs lower-level schedulings for Docker containers to start ZoomSensors as requested.

## 8.1 Clone the Docker Manager Source Code

The source code of the Docker Manager can be found under the [**ZoomSense Docker Manager Repository**](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-docker.git).

Clone the repository:

```
git clone https://gitlab.com/action-lab-aus/zoomsense/zoomsense-docker.git
```

Install dependencies:

```
npm install
```

Here is an overview of the repository structure:

```
.
├── Dockerfile
├── LICENSE
├── README.md
├── db
│   └── admin.js
├── docker
│   └── dockerApi.js
├── docker-compose.yaml
├── index.js
├── logger
│   └── logger.js
├── package-lock.json
└── package.json
```

## 8.2 Create an Admin Account in Firebase with Custom Claims

Under the **Build** section, choose **Authentication** and click **Add user** to start creating the admin user account. Provide the email and password for the account and click **Add user** to finalize the creation:

<img src="../img/quickstart/8_docker/8.2_admin_user.png" alt="Add New User" width="800"/>

You will be able to see the **User UID** after the creation.

Via the Firebase Admin SDK, you can set Custom User Claim for the admin user with security rules to control access. For example, the following code can be used to set the admin Custom User Claim in Node.js:

```JS
// Set admin privilege on the user corresponding to uid.
admin.auth().setCustomUserClaims("User_UID", { admin: true }).then(() => {
  // The new custom claims will propagate to the user's ID token the
  // next time a new one is issued
});
```

More details can be found on the [**Custom User Claim**](https://firebase.google.com/docs/auth/admin/custom-claims) section on Firebase.

## 8.3 Configure Environment Variables

Create a `.env` file under the root directly with the following credentials:

```
FirebaseUrl=https://FIREBASE_DATABASE_URL.firebaseio.com
ZoomsenseUrl=https://FIREBASE_PROJECT_ID.web.app/#
ZoomAppKey=Zoom SDK App Key
ZoomAppSecret=Zoom SDK App Secret
FirebaseKey=Firebase App Key
FirebaseAuthEmail=Firebase Admin Account Auth Email (Step 8.2)
FirebaseAuthPwd=Firebase Admin Account Auth Password (Step 8.2)
FirebaseBucketUrl=FIREBASE_PROJECT_ID.appspot.com
DockerHost=Public IPv4 Address
DockerPort=2375
DockerImage=zoomsense/windowssensor:latest
ProcessRecordings=false
FirebaseAdmin=Firebase Admin Service Account Credentials
```

- For the `DockerHost` variable, please use your machine's public IPv4 address. You use tools such as [WhatIsMyIP](https://www.whatismyip.com/) to find that.
- For the `FirebaseAmin` variable, you need to stringify Firebase Admin SDK JSON credentials retrieved in [Step 1.2](./2_QuickStart_Firebase.md). For example, in JavaScript, you can use the `JSON.stringify()` function to convert the JSON object to a JSON string.

## 8.4 Add Your Account to the Whitelist

Only whitelisted users are allowed to schedule meetings using ZoomSense. To add your account to the whitelist, go to the **Realtime Database** on the Firebase console, create a node `whitelist` under the root, and add your account used for creating the Zoom Account in [Step 2.1](./3_QuickStart_Zoom_OAuth.md) to the whitelist:

<img src="../img/quickstart/8_docker/8.3_whitelist.png" alt="Add Account to Whitelist" width="800"/>

## 8.4 Run the Docker Manager

Pull the ZoomSense Windows Sensor Image:

```
docker pull zoomsense/windowssensor:latest
```

Start the Docker Manager locally with:

```
node index.js
```

[<p align="right">Next: Schedule a ZoomSense Meeting</p>](./10_QuickStart_Schedule.md)
