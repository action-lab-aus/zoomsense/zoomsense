# Release Notes

## Support Recurring Meetings (2021.02.15)

In order to support recurring meetings (which uses the same meeting ID for all meetings) with ZoomSense, we have made major updates for the backend data structure (mainly by adding the start time as part of the key to identify meetings). To enable recurring meeting support, please upgrade the infrastructure components versions accordingly:

- ZoomSense Web Client: v0.4.6 or above
- ZoomSense Windows Sensor: v1.4.3 or above
- ZoomSense Docker Manager: latest
- ZoomSense Functions: latest

## Zoom for GSuite Integration Issue

Zoom internally uses five codes to differentiate meeting types:

```
1 - Instant meeting
2 - Scheduled meeting
3 - Recurring meeting with no fixed time
4 - PMI meeting
8 - Recurring meeting with a fixed time
```

When using Zoom add-on with Google Calendar for creating recurring meetings, the meeting type will be "3 - Recurring meeting with no fixed time" even if you specifically define the end date or the maximum number of occurrences when setting up the meeting, which results in Zoom API not being able to return single meeting objects for each recurring meeting, and thus can not be identified and scheduled by ZoomSense.

In order to fix the issue, you will need to go to the Zoom management console (e.g. https://monash.zoom.us/), navigate to your upcoming meetings, and hover over the recurring meeting for editing:

<img src="../img/RecurringMeetings_1.png" width="650" alt="Zoom Recurring Meetings" />

Under the Recurring meeting section, set the recurrence to enable the fixed time which will essentially change the meeting type to "8 - Recurring meeting with a fixed time".

<img src="../img/RecurringMeetings_2.png" width="400" alt="Edit Zoom Recurring Meetings"/>

## RTDB Mute/Unmute Control (2021.11.11)

Windows ZoomSensor now listens to the `data/audio/{meetingNo}_{startTime}/{displayName}/muteTasks` RTDB node for any mute/unmute commands. More advanced logic can be performed on the Firebase Function or Web Client-end to determine when the muting/unmuting action needs to be performed by ZoomSensors (e.g. by listening to the audio status changes from the `audio` node).

The audio control command class takes three parameters:

- `status`: mute or unmute
- `uid`: Zoom internal user ID (can be found under the `audio` data node)
- `allowUnmute`: define whether allow the users to unmute themselves after being muted

```C#
class AudioCmd
    {
        public AudioCmd(string status, UInt32 uid, string allowUnmute)
        {
            this.status = status;
            this.uid = uid;
            this.allowUnmute = allowUnmute;
        }

        public string status { get; set; }
        public UInt32 uid { get; set; }
        public string allowUnmute { get; set; }
    }
}
```

This feature is available for all ZoomSensor versions after `v1.7.5`.

## Record Emoji Event Logs (2021.10.30)

Windows ZoomSensor is now capable of recording emoji reactions received during Zoom meetings and pushing the data to the RTDB data node (`/data/emoji/{meetingNo}_{startTime}/{displayName}/{key}/emojiData`) with details including emoji type, sender information, and timestamp. Due to the Current Zoom SDK limitation, we can only capture 6 types of emojis (Joy, Clap, Heart, Thumbsup, Openmouth, & Tada).

This feature is available for all ZoomSensor versions after `v1.7.0`.

<img src="../img/Emoji_Log.png" width="250" alt="Emoji Logs"/>

## Record Audio Status Change Event Logs (2021.10.30)

Windows ZoomSensor is now capable of recording audio status changes for participants in Zoom meetings and pushing the data to the RTDB data node (`/data/audio/{meetingNo}_{startTime}/{displayName}/{key}/audioData`) with details including audio status, sender information, and timestamp.

This feature is available for all ZoomSensor versions after `v1.7.0`.

<img src="../img/Audio_Status_Change_Log.png" width="250" alt="Audio Status Change Logs"/>

## Global Messaging (2021.10.07)

The Web Client now allows users to send messages to all Breakout Rooms, as well as receive messages sent directly to ZoomSensors. This allows for two-way communication between users of the Web Client and everybody in a ZoomSense meeting.

![Global Chats](../img/Global%20Chats.png)

## Updated the Colours for Visualisations (2021.10.07)

The colours used for users in the Pie Chart visualisation have been updated.

## ZoomSense Meetings in Sync with Zoom Meetings (2021.10.07)

Previously, when a ZoomSense meeting's underlying Zoom meeting changed, the ZoomSense meeting wouldn't update to reflect this change. Now, ZoomSense meetings actively check their underlying Zoom meeting and keep their details up to date.

## Google Docs Redistribution for Late Joiners (2021.10.07)

Previously, when a participant joined a breakout room after a Google Doc had been distributed in that breakout room, they wouldn't be sent the Google Doc link. Now, when a participant joins a breakout room late, they get messaged with the Google Doc link.

## Enabled Google Doc Distribution by Anonymous Users (2021.09.23)

Previously, Anonymous users were not permitted to distribute Google Docs.

## Allow Meetings to Opt out of Prompts (2021.09.23)

Individual ZoomSense meetings can now opt out of having prompts sent throughout the meeting. Currently, there is no User Interface for this, however, any meetings that request to opt out of prompts via their data structure will be respected.

## Added Whitelist Management to the Admin Panel (2021.09.15)

The Web Client now allows Admins to edit the Whitelist of users who are permitted access to the ZoomSense dashboard.

<img src="../img/Edit%20Whitelist.png" width="350" alt="Edit Whitelist" />

## Download Meeting Data as JSON (2021.09.09)

The Web Client now allows the data for a ZoomSense meeting to be downloaded as a JSON file.

This is only enabled for meetings that have already occurred:

<img src="../img/Download%20Meeting%20Data.png" width="500" alt="Download ZoomSense Meeting Data" />

## Enable Cancelling ZoomSense meetings (2021.09.03)

Upcoming ZoomSense meetings can now be cancelled via the Web Client.

## Enable Editing Count of ZoomSensors (2021.09.03)

The count of sensors on an upcoming ZoomSense meeting can now be edited via the Web Client.

Editing and Deleting ZoomSense meetings are only available for upcoming meetings:

<img src="../img/Delete%20and%20Edit%20ZoomSensor%20Meeting.png" width="500" alt="Delete and Edit ZoomSensor Meeting" />

## Manually Schedule ZoomSense Meetings (2021.08.29)

The Web Client now allows users to manually schedule a ZoomSense meeting, by typing the meeting details. This allows users to schedule ZoomSense for meetings that they didn't create. It also allows ZoomSensors to be added to meetings that have already started.

<img src="../img/Schedule%20ZoomSense%20Manually.png" width="400" alt="ZoomSense Schedule Manually" />

## View ZoomSense Meetings by Date (2021.08.29)

The Web Client now displays a calendar. When the user selects a date on the calendar, only ZoomSense meetings for that date are listed

<img src="../img/View%20ZoomSense%20meeting%20by%20date.png" width="600" alt="ZoomSense View by Date" />

## Enable Webinar Scheduling (2021.08.06)

In order for ZoomSensor to join a Zoom Webinar and successfully retrieve data including active speakers and chat messages (by making the ZoomSensor a [Webinar Panelist](https://support.zoom.us/hc/en-us/articles/360000252726-Roles-in-a-Zoom-Webinar)), a webinar-specific workflow has been added to the ZoomSense infrastructure. Since breakout rooms are not supported by webinars, we only allow scheduling one sensor for each Zoom webinar.

To support the Webinar workflow, the following updates have been made to the infrastructure:

### Fetch Zoom Webinars

- When fetching the user's scheduled Zoom meetings via the web client, the `getScheduledMeetings` function will be triggered. Instead of just calling `getZoomMeetings`, the `getZoomWebinars` function has been added using the Zoom API's [List Webinars Endpoint](https://marketplace.zoom.us/docs/api-reference/zoom-api/methods#operation/webinars)
- For recurring webinars, call the `getMeetingDetails` function under `auth.js` to fetch webinars' start time and end time
- Return the full list of meetings (both regular Zoom meetings and webinars) back to the web client for scheduling

### Schedule Zoom Webinars

- When scheduling a Zoom Webinar session, the `getMeetingPassword` function under `auth.js` will be able to fetch the `isWebinar` flag from the request body. If it is a webinar, call the `addWebinarPanelist` function under `auth.js` to add **ZoomSensor_1** as a panelist for the current webinar (using the Zoom API's [Add Panelists Endpoint](https://marketplace.zoom.us/docs/api-reference/zoom-api/methods#operation/webinarPanelistCreate))
- When adding a new panelist for a webinar, an email will be sent to the new panelist with the webinar access token:

<img src="../img/release/panelist_email.png" width="650" alt="Panelist Email" />

- In Amazon SES (`us-east-1`), we defined an email receiving rule that for all the inbounding emails for the dedicated ZoomSense mailbox, we deliver the content to Amazon S3 Bucket and invoke an [AWS Lambda Function](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-functions/-/tree/master/zoomsense-lambda) to parse the email content

<img src="../img/release/zoomsense_inbound_emails.png" width="650" alt="Amazon SES - ZoomSense Inbound Emails" />

- After successfully fetching the meeting id & webinar token from the email body, the Lambda function will make a POST request to the Firebase `webinarToken` function, which updates the meeting data node with the webinar token
- When the webinar is scheduled in the ZoomSense infrastructure, the webinar token will get copied to the `scheduling` data node and the Docker Manager will pass on the webinar token to the ZoomSensor for it to join the webinar as a panelist
  - [ZoomSense Windows Updates for the Webinar Workflow](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-windows/-/commit/33b4ffce2d0fa65116549624f61da738b33e43d5)
  - [Docker Manager Updates for the Webinar Workflow](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-docker/-/commit/f1343a8eab43206e7e2ef49bad45bb3dcffce01c)

## Enable Recording in Main Rooms by ZoomSensor (2021.02.15)

Although ZoomSensors are capable of recording breakout rooms and pushing the recordings to Firebase Storage automatically, main room recordings were not supported by the ZoomSense infrastructure previously. We have now introduced the feature update to support this. By specifically allowing the manager sensor (ZoomSensor_1) to record locally, all main room sessions will be captured and uploaded as breakout rooms.
