[<p align="left">Previous: Zoom SDK App</p>](./4_QuickStart_Zoom_SDK.md)

# 4. Configure Firebase CLI

Running and deploying Firebase services requires the installation of the [**Firebase CLI**](https://firebase.google.com/docs/cli).

## 4.1 Install Firebase CLI

Run the following npm command to install the CLI or update to the latest CLI version.

```
npm install -g firebase-tools
```

If using Windows, Git Bash doesn't work - Windows Powershell seems to work best. If you're using Windows Powershell, you will probably have to run the following command each session you want to use the Firebase CLI:

```
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass
```

## 4.2 Sign in to Google

Run the following command to sign in to Google.

```
firebase login
```

[<p align="right">Next: Deploy ZoomSense Web Client</p>](./6_QuickStart_Web_Client.md)
