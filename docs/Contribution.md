# Contribution Guide

If you feel you can contribute - please get in touch! We would love to partner with other developers who see the value in ZoomSense.

## Authentication for Developers to Access ZoomSense Data

Each developer will be provided with a Firebase email/password account for working with live data from an installation. The Firebase account will be assigned with [Custom Token Claim](https://firebase.google.com/docs/auth/admin/custom-claims) with the `Developer` role.

Data generated by Windows ZoomSensors is just one type of data in the ZoomSense infrastructure. There could be many more including both raw and inferred data which can be integrated with ZoomSense. To reduce the burden on the Firebase RTDB, the information about participants and the timestamp for groups (i.e. timestamp for opening and closing breakout rooms) will be stored separately under the `meetings` node as references for all types of data.

- **READ Permission**: Developer accounts will have read access for ZoomSense raw data under the `data` node including `activeSpeakers` & `chats`. Read access for the `meeting` node will also be provided to get the session references.

- **WRITE Permission**: Developer accounts will have write access to push the inferred data back with the following structure to match with the sessions used by the raw data. Only the data that the developer account owns can be pushed back to the Firebase. All inferred data will be pushed under the top-level `data` node to ensure different types of data will all be stored together.

```JSON
"data": {
    "inferredData": {
        "meetingid": {
            "sensorid": {
                "sessionTimestamp": {
                    "actualData": {
                        // Your Actual Inferred Data
                    }
                }
            }
        }
    }
}
```

## Install Your Own Infrastructure for Development

If you want to set up your own ZoomSense infrastructure for development and deployment, please follow the [Quick Start Guide](./1_QuickStart_Instructions.md) to get started running your own installation.

# Development Pipeline

In order to contribute to ZoomSense, simply create a new branch and work on your new features or adjustments. Once you are happy with your progress, submit a merge request to the master branch to include your updates in the main repository.
