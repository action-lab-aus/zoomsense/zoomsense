[<p align="left">Next: Configure Docker Manager & Windows Sensors</p>](./9_QuickStart_Docker.md)

# 9. Schedule a ZoomSense Meeting

This section provides a quick overview of the configuration updates on Zoom and how to schedule meetings to be used for ZoomSense.

## 9.1 Change Your Default Zoom Meeting Time Zone

To schedule a ZoomSense meeting, you need to create a meeting on [Zoom](https://zoom.us/) first.

Sign in to Zoom with your account, and go to the **Profile** tab. Under the **Personal** section, change the default Time Zone to be your local time zone.

<img src="../img/quickstart/9_schedule/9.1_timezone.png" alt="Change Your Default Time Zone" width="800"/>

## 9.2 Schedule a Meeting on Zoom

Under the **Meetings** tab, click **Schedule a Meeting**:

<img src="../img/quickstart/9_schedule/9.2.1_meetings.png" alt="Schedule a Meeting" width="800"/>

Fill in the details for your Zoom Meeting and click **Save** to create the meeting:

<img src="../img/quickstart/9_schedule/9.2.2_schedule.png" alt="Schedule a Meeting" width="800"/>

## 9.3 Schedule a ZoomSense Meeting

After scheduling the Zoom Meeting, you can go to the ZoomSense Web Client to schedule ZoomSense meetings. By defining the number of expected Breakout Rooms in the Zoom meeting and clicking the add button, ZoomSensors will be scheduled by the serverless backend automatically and will join the meeting when it starts.

<img src="../img/quickstart/9_schedule/9.3.1_zoomsense_schedule.png" alt="Schedule ZoomSense Meetings" width="800"/>

ZoomSense Web Client also provides an implementation of visualizing the data to let the meeting hosts to get a basic understanding of what's happening in your Zoom meetings.

<img src="../img/quickstart/9_schedule/9.3.2_zoomsense_meetings.png" alt="Scheduled ZoomSense Meeting" width="800"/>

[<p align="right">Next: Configure the Live Environment</p>](./11_QuickStart_VM.md)
